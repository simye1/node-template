const Transport = require('../transport/todo');

exports.toTransport = (model) => {
    // map model object to transport object
    // extra transformation logic live here
    return new Transport(model.id, model.title, model.description, model.status);
}