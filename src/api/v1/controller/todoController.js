// const todoService = require('../../../service/todoService');
const mapper = require('../mapper/mapper');
const Todo = require('../../../model/todo');

// exports.listTodos = async (req, res, next) => {
//   console.info('Listing all todos');
//   try {
//     const todos = await todoService.listTodo();
//     const transports = todos.map(mapper.toTransport);
//     res.status(200).json(transports);
//   } catch (e) {
//     console.error('There is error while listing todos', e);
//     next(e);
//   }
// }

// exports.getTodo = async (req, res, next) => {
//   const todoId = req.params.todoId;
//   console.info("Getting todo by id");
//   try {
//     const todo = await todoService.getTodo(todoId);
//     const transport = mapper.toTransport(todo);
//     res.status(200).json(transport);
//   } catch (e) {
//     console.error(`There is error while getting todo ${todoId}`, e);
//     next(e);
//   }
// }

// exports.markDone = async (req, res, next) => {
//   const todoId = req.params.todoId;
//   try {
//     const updated = await todoService.markDone(todoId);
//     res.status(200).json(mapper.toTransport(updated));
//   } catch (e) {
//     console.error(`There is error while mark done todo ${todoId}`, e);
//     next(e);
//   }
// }

// exports.createTodo = async (req, res, next) => {
//   console.info('Creating new todos');
//   try {
//     const todo = new Todo(null, req.body.title, req.body.description, 'TODO');
//     const created = await todoService.createTodo(todo);
//     res.location('/todos/' + created.id);
//     res.status(201).json(mapper.toTransport(todo));
//   } catch (e) {
//     next(e);
//   }
// }

class TodoController {
  constructor(todoService) {
    this.todoService = todoService;
  }

  async listTodos(req, res, next) {
    console.info('Listing all todos');
    try {
      const todos = await this.todoService.listTodo();
      const transports = todos.map(mapper.toTransport);
      res.status(200).json(transports);
    } catch (e) {
      console.error('There is error while listing todos', e);
      next(e);
    }
  }

  async getTodo(req, res, next) {
    const todoId = req.params.todoId;
    console.info("Getting todo by id");
    try {
      const todo = await this.todoService.getTodo(todoId);
      const transport = mapper.toTransport(todo);
      res.status(200).json(transport);
    } catch (e) {
      console.error(`There is error while getting todo ${todoId}`, e);
      next(e);
    }
  }

  async markDone(req, res, next) {
    const todoId = req.params.todoId;
    try {
      const updated = await this.todoService.markDone(todoId);
      res.status(200).json(mapper.toTransport(updated));
    } catch (e) {
      console.error(`There is error while mark done todo ${todoId}`, e);
      next(e);
    }
  }

  async createTodo(req, res, next) {
    console.info('Creating new todos');
    try {
      const todo = new Todo(null, req.body.title, req.body.description, 'TODO');
      const created = await this.todoService.createTodo(todo);
      res.location('/todos/' + created.id);
      res.status(201).json(mapper.toTransport(todo));
    } catch (e) {
      next(e);
    }
  }

}

module.exports = TodoController