'use strict';
const TodoSequelize = require('./sequelize/dbSequelize').Todo;
const Op = require('sequelize').Op;

// exports.listTodos = async () => {
//   return (await TodoSequelize.findAll({}).then()).map(toResponse);
// }

// exports.getTodo = async (id) => {
//   return await TodoSequelize.findByPk(id).then(data => toResponse(data));
// }

// exports.createTodo = async (todo) => {
//     return await TodoSequelize.create({
//       id: todo.id,
//       title: todo.title,
//       description: todo.description,
//       status: todo.status
//     }).then(data => toResponse(data));
// }

// exports.updateTodo = async (todo) => {
//   const condition = {
//     where: {
//       id: todo.id
//     }
//   };
//   return TodoSequelize.update(todo, condition).then(data => toResponse(data));
// }

// exports.deleteTodo = async (id) => {

// }

const toResponse = (todoSequelize) => {
  return {
    id: todoSequelize.id,
    title: todoSequelize.title,
    description: todoSequelize.description,
    status: todoSequelize.status
  }
}

class TodoSQLDataAdapter {
  constructor() {
    // intentional
  }

  async listTodos() {
    return (await TodoSequelize.findAll({}).then()).map(toResponse);
  }

  async getTodo(id) {
    return await TodoSequelize.findByPk(id).then(data => toResponse(data));
  }

  async createTodo(todo) {
    return await TodoSequelize.create({
      id: todo.id,
      title: todo.title,
      description: todo.description,
      status: todo.status
    }).then(data => toResponse(data));
  }

  async updateTodo(todo) {
    const condition = {
      where: {
        id: todo.id
      }
    };
    return TodoSequelize.update(todo, condition).then(data => toResponse(data));
  }

  async deleteTodo(id) {

  }
}

module.exports = TodoSQLDataAdapter